Source: orc
Section: devel
Priority: optional
Maintainer: Maintainers of GStreamer packages <orc@packages.debian.org>
Uploaders: Sebastian Dröge <slomo@debian.org>,
           Sjoerd Simons <sjoerd@debian.org>,
           Marc Leeman <marc.leeman@gmail.com>
Build-Depends: dpkg-dev (>= 1.22.5),
               debhelper-compat (= 13),
               meson,
               pkgconf,
               gtk-doc-tools
Rules-Requires-Root: no
Standards-Version: 4.6.2
Homepage: https://cgit.freedesktop.org/gstreamer/orc
Vcs-Git: https://salsa.debian.org/gstreamer-team/orc.git
Vcs-Browser: https://salsa.debian.org/gstreamer-team/orc/

Package: liborc-0.4-0t64
Provides: ${t64:Provides}
Replaces: liborc-0.4-0
Breaks: liborc-0.4-0 (<< ${source:Version})
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: Library of Optimized Inner Loops Runtime Compiler
 Orc is a library and set of tools for compiling and executing
 very simple programs that operate on arrays of data.  The "language"
 is a generic assembly language that represents many of the features
 available in SIMD architectures, including saturated addition and
 subtraction, and many arithmetic operations.
 .
 This package contains the orc shared libraries.  It is typically
 installed automatically when an application or library requires it.

Package: liborc-0.4-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: liborc-0.4-0t64 (= ${binary:Version}),
         liborc-0.4-dev-bin:any (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Provides: liborc-dev
Suggests: liborc-0.4-doc
Description: Library of Optimized Inner Loops Runtime Compiler (development headers)
 Orc is a library and set of tools for compiling and executing
 very simple programs that operate on arrays of data.  The "language"
 is a generic assembly language that represents many of the features
 available in SIMD architectures, including saturated addition and
 subtraction, and many arithmetic operations.
 .
 This package contains the development headers and libraries, and should
 be installed to compile software that uses orc.

Package: liborc-0.4-dev-bin
Section: libdevel
Architecture: any
Multi-Arch: allowed
Depends: ${misc:Depends}, ${shlibs:Depends}
Provides: liborc-dev-bin
Description: Library of Optimized Inner Loops Runtime Compiler (development tools)
 Orc is a library and set of tools for compiling and executing
 very simple programs that operate on arrays of data.  The "language"
 is a generic assembly language that represents many of the features
 available in SIMD architectures, including saturated addition and
 subtraction, and many arithmetic operations.
 .
 This package contains the development tools, and should not be
 installed directly.

Package: liborc-0.4-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Suggests: liborc-0.4-dev
Description: Library of Optimized Inner Loops Runtime Compiler (documentation)
 Orc is a library and set of tools for compiling and executing
 very simple programs that operate on arrays of data.  The "language"
 is a generic assembly language that represents many of the features
 available in SIMD architectures, including saturated addition and
 subtraction, and many arithmetic operations.
 .
 This package contains the documentation for orc.
